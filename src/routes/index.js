import Vue from "vue";
import Router from "vue-router";
import LandingPage from "../views/guests/LandingPage";
import Home from "../views/Home";
import Authentication from "../views/guests/Authentication";
import Register from "../views/guests/authentication/Register";
import Login from "../views/guests/authentication/Login";
import Verification from "../views/users/verification/Verification";
import ClientHome from "../views/users/client/ClientHome";
import ClientDashboard from "../views/users/client/views/ClientDashboard";
import ClientProfile from "../views/users/client/views/ClientProfile";
import ClientDelivery from "../views/users/client/views/ClientDelivery";
import ClientWallet from "../views/users/client/views/ClientWallet";
import ClientCreateDelivery from "../views/users/client/views/ClientCreateDelivery";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "",
      component: Home,
      meta: {
        requireAuth: false
      },
      children: [
        {
          path: "",
          name: "landing-page",
          component: LandingPage
        },
        {
          path: "authentication",
          component: Authentication,
          meta: {
            requireAuth: false
          },
          children: [
            {
              path: "register",
              name: "register",
              component: Register
            },
            {
              path: "login",
              name: "login",
              component: Login
            }
          ]
        }
      ]
    },
    {
      path: "/verification",
      name: "verification",
      component: Verification,
      meta: {
        requireAuth: true
      }
    },
    {
      path: "/client",
      component: ClientHome,
      meta: {
        requireAuth: true
      },
      children: [
        {
          path: "/dashboard",
          name: "client-dashboard",
          component: ClientDashboard
        },
        {
          path: "/delivery",
          name: "client-deliveries",
          component: ClientDelivery
        },
        {
          path: "/wallet",
          name: "client-wallet",
          component: ClientWallet
        },
        {
          path: "/profile",
          name: "client-profile",
          component: ClientProfile
        },
        {
          path: "/complaints",
          name: "client-create-complaints"
        },
        {
          path: "/creat",
          name: "client-create-delivery",
          component: ClientCreateDelivery
        }
      ]
    }
  ]
});
