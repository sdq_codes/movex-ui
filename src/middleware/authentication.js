export let authentication = (to, from, next) => {
  let userJson = localStorage.getItem("user");
  if (to.meta.requireAuth && userJson) {
    next();
  } else if (to.meta.requireAuth && !userJson) {
    this.$router.push({ name: "login" });
  } else {
    next();
  }
};
